﻿using Entyties;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections;
using System.Net;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            //Подключим клиента к API
            using RestClient restClient = new("https://localhost:5001");

            var cycle = true;

            while (cycle)
            {
                Console.WriteLine("Домашняя работа к уроку 7\n");
                Console.WriteLine("Навигация:\n" +
                    "1 - Список всех клиентов\n" +
                    "2 - Информация о клиенте по ID\n" +
                    "3 - Добавить нового (случайного) клиента\n" +
                    "q - Выход");
                ConsoleKeyInfo key = Console.ReadKey();
                Console.WriteLine();
                switch (key.KeyChar)
                {
                    case '1':
                        {
                            ICustomer client = null;
                            var restRequest = new RestRequest("/customers", Method.Get);
                            var result = (await restClient.ExecuteAsync(restRequest)).Content;
                            IList list = JsonConvert.DeserializeObject<IList>(result);
                            if (list == null || list.Count==0)
                            {
                                Console.WriteLine("В базе нет клиентов\n");
                                break;
                            }

                            foreach (var item in list)
                            {
                                client = Customer.CustomerFromJsonString(item.ToString());
                                client.Print();
                            }
                        }
                        break;

                    case '2':
                        {
                            Console.Write("\nВведите идентификатор клиента: ");
                            var id_s = Console.ReadLine();
                            Console.WriteLine();
                            int id = 0;
                            if (int.TryParse(id_s, out int _id))
                            {
                                id = _id;
                            }
                            else
                            {
                                Console.WriteLine("\nВы ввели некоректное значение ID");
                                break;
                            }
                            var restRequest = new RestRequest($"/customers/{id}", Method.Get);
                            dynamic result = await restClient.ExecuteAsync(restRequest);
                            switch (result.StatusCode)
                            {
                                case HttpStatusCode.OK:
                                    ICustomer client = Customer.CustomerFromJsonString(result.Content);
                                    client.Print();
                                    break;
                                case HttpStatusCode.NoContent:
                                    Console.WriteLine("Клиент с таким идентификатором не найден");
                                    break;
                                case HttpStatusCode.InternalServerError:
                                    Console.WriteLine("Что-то пошло не так....");
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;

                    case '3':
                        try
                        {
                            ICustomer newCustomer = new Customer();
                            var restRequest3 = new RestRequest($"/customers", Method.Post);
                            restRequest3.AddJsonBody(newCustomer);
                            dynamic response = await restClient.ExecuteAsync(restRequest3);
                            ICustomer customer = Customer.CustomerFromJsonString(response.Content);
                            customer.Print();
                        }
                        catch
                        {
                            Console.WriteLine("Что-то пошло не так....");
                        }
                        break;

                    case 'q':
                        cycle = false;
                        break;

                    default:
                        break;
                }
            }

            restClient.Dispose();

        }

    }
}