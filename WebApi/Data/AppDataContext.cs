﻿using Entyties;
using Microsoft.EntityFrameworkCore;


namespace WebApi.Data
{
    /// <summary>
    /// Контекст БД
    /// </summary>
    public class AppDataContext:DbContext
    {
        /// <summary>
        /// Конструктор 
        /// </summary>
        /// <param name="options">Параметры контекста БД</param>
        public AppDataContext(DbContextOptions<AppDataContext> options):base(options)
        {
            //Database.EnsureDeleted(); //Для отладки и очистки БД
            Database.EnsureCreated();   // создаем базу данных если е нет
        }

        /// <summary>
        /// Покупатели
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
    }
}
