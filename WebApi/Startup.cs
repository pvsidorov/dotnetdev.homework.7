using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using WebApi.Data;

namespace WebApi
{
#pragma warning disable 1591
    public class Startup
    {
        private readonly IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDataContext>(options =>
            {
                options.UseSqlite(Configuration.GetConnectionString("AppDataBase"));
                options.EnableDetailedErrors();
            });

            services.AddControllers().AddJsonOptions(o =>
   o.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve); ;

            services.AddSwaggerGen(options =>
            {
                //������� OpenAPI
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "�������� ������ � ����� 7",
                    Description = "��������� �������������� ����� �������� � ��������",
                    Contact = new OpenApiContact
                    {
                        Name = "Pavel Sidorov",
                        Email = "pvsidorov@gmail.com"
                    }
                });

                //���������� XML ������������ � �������
                var xml = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xml));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                //Swagger ������ � ������ ������
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = string.Empty;
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                });
            }

            app.UseRouting();
            app.UseStaticFiles();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
#pragma warning restore 1591
}