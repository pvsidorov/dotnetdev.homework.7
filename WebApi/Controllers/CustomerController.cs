using Entyties;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WebApi.Data;


namespace WebApi.Controllers
{
    /// <summary>
    /// ����������
    /// </summary>
    [ApiController]
    [Route("customers")]
    [Produces("application/json")]
    public class CustomerController : ControllerBase
    {
        private readonly AppDataContext db;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="_db">�������� ��, ���������� DI �� Services</param>
        public CustomerController(AppDataContext _db)
        {
            db = _db;
        }

        /// <summary>
        /// ������ �����������
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCustomersListAsync()
        {
            try
            {
                var customers = await db.Customers.ToListAsync();
                if (customers == null)
                    return NoContent();
                else
                    return Ok(customers);
            }
            catch (Exception ex)
            {
#if DEBUG
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
#else
                return StatusCode(StatusCodes.Status500InternalServerError);
#endif
            }
        }


        /// <summary>
        /// ���������� � ����������
        /// </summary>
        /// <param name="id">������������� �������</param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCustomerAsync(long id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            try
            {
                var customer = await db.Customers.FindAsync(id);

                if (customer == null)
                    return NoContent();
                return Ok(customer);
            }
            catch (Exception ex)
            {
#if DEBUG
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
#else
                return StatusCode(StatusCodes.Status500InternalServerError);
#endif
            }
        }

        /// <summary>
        /// ������� ������ ����������
        /// </summary>
        /// <param name="customer">������ ����������</param>
        /// <returns></returns>
        /// <remarks>
        /// POST /customer
        /// {
        ///     "firstName": "����",
        ///     "lastName": "������"
        /// }
        /// </remarks>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateCustomerAsync(Customer customer)

        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var id = db.Customers.Add(new Customer(firstName:customer.FirstName, lastName:customer.LastName));

                var res = await db.SaveChangesAsync();
                return Created($"/customer/{id.Entity.Id}",
                    new Customer
                    (
                        id : id.Entity.Id,
                        firstName : customer.FirstName,
                        lastName : customer.LastName
                    ));
            }
            catch (Exception ex)
            {
#if DEBUG
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
#else
                return StatusCode(StatusCodes.Status500InternalServerError);
#endif
            }
        }
    }
}