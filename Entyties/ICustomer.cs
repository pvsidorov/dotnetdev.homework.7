﻿namespace Entyties
{
    public interface ICustomer
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        long Id { get; init; }

        /// <summary>
        /// Имя
        /// </summary>
        string FirstName { get; init; }

        /// <summary>
        /// Фамилия
        /// </summary>
        string LastName { get; init; }

        /// <summary>
        /// Метод вывода на консоль
        /// </summary>
        void Print();
    }
}