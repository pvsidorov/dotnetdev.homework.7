﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Entyties
{
    /// <summary>
    /// Модель покупателя
    /// </summary>
    public class Customer : ICustomer
    {
        /// <summary>
        /// Конструктор по умолчанию генерит случайного клиента
        /// </summary>
        public Customer()
        {
            var obj = RandomCustomer();
            FirstName = obj.FirstName;
            LastName = obj.LastName;
        }

        public Customer(string firstName, string lastName)
        {

            FirstName = firstName;
            LastName = lastName;
        }

        public Customer(long id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }



        /// <summary>
        /// Идентифкатор
        /// </summary>
        [Key]
        public long Id { get; init; }

        /// <summary>
        /// Имя
        /// </summary>
        [Required]
        public string FirstName { get; init; } = "";

        /// <summary>
        /// Фамилия
        /// </summary>
        [Required]
        public string LastName { get; init; } = "";


        /// <summary>
        /// Преобразование полученной строки JSON в объект
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public static ICustomer CustomerFromJsonString(string jsonString)
        {
            ICustomer customer;
            try { 
            dynamic newObject = JsonConvert.DeserializeObject(jsonString);
            if (newObject == null)
                return null;
                customer = new Customer() { Id = newObject.id, FirstName = newObject.firstName, LastName = newObject.lastName };
            }
            catch
            {
                throw new InvalidCastException("Не удалось преобразовать строку");
            }

            return customer;
        }

        /// <summary>
        /// Вывод данных покупателя на консоль
        /// </summary>
        public void Print()
        {
            Console.WriteLine($"ID : {Id}");
            Console.WriteLine($"Фамилия : {LastName}");
            Console.WriteLine($"Имя : {FirstName}\n");
        }

        /// <summary>
        /// Генерируем случайные ФИ
        /// </summary>
        /// <returns></returns>
        private static Customer RandomCustomer()
        {
            List<string> femaleNames = new() { "Ирина", "Елена", "Анна", "Анастасия", "Евгения", "Ольга", "Инна", "Светлана", "Ираида", "Надежда" };
            List<string> femaleSurnames = new() { "Иванова", "Петрова", "Сидорова", "Булаева", "Пушкина", "Тютчева", "Некрасова", "Свиридова", "Лермонтова", "Солженицена" };
            List<string> maleNames = new() { "Иван", "Петр", "Павел", "Василий", "Николай", "Валерий", "Вячеслав", "Михаил", "Дмитрий", "Виктор" };
            List<string> maleSurnames = new() { "Маргелов", "Цой", "Жуков", "Пушкин", "Толстой", "Сидоров", "Кипелов", "Суминов", "Островский", "Веселов" };

            Customer customer;

            Random rand = new();
            if (rand.Next(1, 10) % 2 == 1)
            {
                customer = new Customer
                (
                    firstName: maleNames[rand.Next(0, maleNames.Count - 1)],
                    lastName: maleSurnames[rand.Next(0, maleNames.Count - 1)]
                );

            }
            else
            {
                customer = new Customer
                 (
                     firstName: femaleNames[rand.Next(0, femaleNames.Count - 1)],
                     lastName: femaleSurnames[rand.Next(0, femaleSurnames.Count - 1)]
                 );
            }

            return customer;
        }
    }



}